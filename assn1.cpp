//---------------------------
// Programming Assignment #1
// Victor Zordan
//
// Cooper Wolfe (cswolfe)
// Due 10/4/17
//---------------------------

// Include needed files
#include <GL/gl.h>
#include <GL/glu.h>
#include <stdlib.h>
#include <stdio.h>
#include <math.h>
#include <GL/glut.h> // The GL Utility Toolkit (Glut) Header

#define WIDTH 500
#define HEIGHT 500
#define FPM 60 // Frames per move

// Create types
struct Point {
	int x, y;
	Point(int _x, int _y) { x = _x; y = _y; }
};
struct BezierCurve {
	int numPoints;
	Point * points[4];
	BezierCurve() {
		numPoints = 0;
		for (int i = 0; i < 4; ++i)
			points[i] = NULL;
	}
};

// Initialize global data structures and variables
BezierCurve *curve = new BezierCurve();
Point * selectedPoint = NULL;
bool moving;
int dx[FPM], dy[FPM];
int selectedX, selectedY;

// Clear the image area, and set up the coordinate system
void init_window()
{
	// Clear window
	glClearColor(0.0,0.0,0.0,0.0);
	glShadeModel(GL_SMOOTH);
	glOrtho(0,WIDTH,0,HEIGHT,-1.0,1.0);
}

// Turn on the pixel found at x,y
void write_pixel(int x, int y, double intensity)
{
	glColor3f (intensity, intensity, intensity);
	glBegin(GL_POINTS);
	glVertex3i( x, y, 0);
	glEnd();
}

// Returns distance between p1 and p2
double distance(Point * p1, Point * p2) {
	double x = p2->x - p1->x, y = p2->y - p1->y;
	return sqrt(x*x + y*y);
}

// Adds point p to curve
void addPoint(Point *p) {
	if (curve->numPoints >= 4) return;
	curve->points[curve->numPoints++] = p;
}

// Sets animation up to move selected point to p
void moveSelectedPoint(Point * p) {
	moving = true;
	// calculate next FPM frames
	for (int i = 0; i < FPM; ++i) {
		dx[i] = selectedPoint->x + (p->x - selectedPoint->x) * (i+1) / FPM;
		dy[i] = selectedPoint->y + (p->y - selectedPoint->y) * (i+1) / FPM;
	}
}

// Returns a point in curve close to p
Point * pointNear(Point * p) {
	// Get nearest point and distance
	Point * nearest = curve->points[0];
	double nearestDist = distance(nearest, p);
	for (int i = 1; i < 4; ++i) {
		Point * curr = curve->points[i];
		double currDist = distance(curr, p);
		if (currDist < nearestDist) {
			nearest = curr;
			nearestDist = currDist;
		}
	}

	// Only return nearest if it is within 20px
	return nearestDist <= 20 ? nearest : NULL;
}

// Draws point as diamond with given radius
void drawPoint(Point * p, int radius) {
	if (radius == 1) return write_pixel(p->x, p->y, 1.0);
	drawPoint(new Point(p->x+1, p->y), radius-1);
	drawPoint(new Point(p->x, p->y+1), radius-1);
	drawPoint(new Point(p->x-1, p->y), radius-1);
	drawPoint(new Point(p->x, p->y-1), radius-1);
}

// Draws control points for curve
void drawPoints() {
	for (int i = 0; i < 4; ++i) {
		Point * curr = curve->points[i];
		if (curr) drawPoint(curr, curr == selectedPoint ? 4 : 3);
	}
}

// Calculates the factorial of i
int fact(int i) {
	return i > 1 ? i * fact(i-1) : 1;
}

// Calculates the c_i for the Bezier curve formula
double c(int i) {
	return fact(3) / (fact(i) * fact(3-i));
}

// Calculates f_i(u) for the Bezier curve formula
double f_i(int i, double u) {
	return c(i) * pow(u, i) * pow(1-u,3-i);
}

// Calculates the point on the curve at u | 0 < u < 1
Point * p(double u) {
	Point * p = new Point(0, 0);
	for (int i = 0; i < 4; ++i) {
		Point * curr = curve->points[i];
		double f = f_i(i, u);
		p = new Point(p->x + curr->x*f, p->y + curr->y*f);
	}
	return p;
}

// Draws curve with u-value between lo and hi (implemented for recursion)
void drawCurveBetween(double lo, double hi) {
	double u = (hi + lo) / 2.0;
	if (u == hi || u == lo) return;
	Point * curr = p(u);
	if (distance(curr, p(lo)) > 1.0) drawCurveBetween(lo, u);
	if (distance(curr, p(hi)) > 1.0) drawCurveBetween(u, hi);
	drawPoint(p(u), 1);
}

// Create the display function
void display()
{
	// Clear Screen
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	// Move selected point to calculated frames
	static int i = 0;
	if (moving) {
		selectedPoint->x = dx[i];
		selectedPoint->y = dy[i];
		if (++i == FPM) {
			i = moving = false;
			selectedPoint = NULL;
		}
	}

	// Draw
	drawPoints();
	if (curve->numPoints == 4)
		drawCurveBetween(0.0, 1.0);

	// Draw Frame Buffer
	glutSwapBuffers();
}

/* This function I finessed a bit, the value of the printed x,y should
   match the screen, also it remembers where the old value was to avoid multiple
   readings from the same mouse click.  This can cause problems when trying to
   start a line or curve where the last one ended */
void mouse(int button, int state, int x, int y)
{
	// Don't read mousedown, only mouseup while not moving.
	if (state == 0 || moving) return;

	y *= -1;  //align y with mouse
	y += 500; //ignore

	// Decide what to do with click:
	Point * p = new Point(x, y);
	if (curve->numPoints < 4) addPoint(p); // Use for curve,
	else if (selectedPoint)	moveSelectedPoint(p); // Move selected,
	else selectedPoint = pointNear(p); // or Select
}

// Create Keyboard Function
void keyboard (unsigned char key, int x, int y)
{
	switch (key) {
		case 27: // ESC
			exit (0);
		default:
			break;
	}
}

/* This main function sets up the main loop of the program and continues the
   loop until the end of the data is reached. Then the window can be closed
   using the escape key. */
int main (int argc, char *argv[])
{
	glutInit(&argc, argv);
	glutInitDisplayMode(GLUT_RGB | GLUT_DOUBLE | GLUT_DEPTH);
	glutInitWindowSize(500, 500);
	glutCreateWindow("Computer Graphics");
	glutDisplayFunc(display);
	glutIdleFunc(display);
	glutMouseFunc(mouse);
	glutKeyboardFunc(keyboard);

	// Create window
    init_window();

	// Initialize The Main Loop
	glutMainLoop();
}
